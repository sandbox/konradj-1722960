-- SUMMARY --

The Varnish Kit module contains three varnish add on modules

* Varnish Bans
  - The Varnish bans module gives you the options to manually invalidate
    content currently in you varnish cache. The Varnish module combined 
    with the expire module helps you keep your site updated daily. This
    module helps you when you make changes to your page, which expire module
    does not automatically update.

* Varnish Max-age
  - The Varnish maxage module gives you the options to manually set s-maxage
    and max-age on content.

* Varnish Multi domain  
  - The Varnish multi domain module gives you the options to add support for
    updating multiple domains. its main purpose is to update your real
    domain when your admin pages are located under a second secure URL


For a full description of the module, visit the project page:
  http://drupal.org/project/1722960/  

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/node/add/project-issue/1722960

-- REQUIREMENTS --

The varnish module
  http://drupal.org/project/varnish


-- INSTALLATION --

* Install as usual, see for further information.
  http://drupal.org/documentation/install/modules-themes/modules-7 


-- CONFIGURATION --

* None


-- TROUBLESHOOTING --

* If pages don't updates after a ban request

  - Check the varnish module configuration, to see if the connection to all
    the varnish servers are OK

  - Log inn to the varnish server, and check that the correct ban commands
    gets to the server. 
    eks: varnishlog | grep "Rd ban"


-- FAQ --

* None yet

-- CONTACT --

Current maintainers:
* Korad Jørgensen - http://drupal.org/user/1281714

This project has been sponsored by:
* Aller Media AS
  Aller Media is part of the Nordic Aller Group, owned 100% of the
  Danish Aller Holding A/S. With over 110 years experience we are
  one of the oldest and most traditional media companies.
  Visit http://www.aller.no/ for more information.
