<?php
/**
 * @file
 * Use this script to check the drupal backend helth, from Varnsih.
 *
 * Exampel for varnish conf:

probe healthcheck {
  .request =
    "GET /sites/all/modules/custom/varnish_kit/backend_check.php HTTP/1.1"
    "Host: www.site.no"
    "Connection: close";
  .interval = 5s;
  .timeout = 100ms;
  .window = 5;
  .threshold = 3;
}

# A cluster of backends selected in a round-robin manner.
director default round-robin {
  { .backend = {
      .host = "web01.site.no";
      .port = "http";
      .probe = healthcheck;
      .connect_timeout = 1s;
      .first_byte_timeout = 20s;
      .between_bytes_timeout = 5s;
      .saintmode_threshold = 20;
      .max_connections = 50;
  } }
  { .backend = {
      .host = "web02.site.no";
      .port = "http";
      .probe = healthcheck;
      .connect_timeout = 1s;
      .first_byte_timeout = 20s;
      .between_bytes_timeout = 5s;
      .saintmode_threshold = 20;
      .max_connections = 50;
  } }
}

 *
 * To check the helt use the Varnish CLI comand debug.health
 *
 * Tips:
 * - evil backend hack https://www.varnish-software.com/static/book/Saving_a_request.html
 * - restart on errors https://www.varnish-cache.org/trac/wiki/VCLExampleRestarts
 * - saintmode https://www.varnish-cache.org/trac/wiki/VCLExampleSaintMode
 */

/*
 * Register our shutdown function so that no other shutdown functions run before this one.
 * This shutdown function calls exit(), immediately short-circuiting any other shutdown functions,
 * such as those registered by the devel.module for statistics.
 */
register_shutdown_function('status_shutdown');
function status_shutdown() {
  exit();
}

// Build up our list of errors.
$errors = array();

// Set root path based on SCRIPT_FILENAME.
$path = explode('/sites/all', $_SERVER['SCRIPT_FILENAME']);
define('DRUPAL_ROOT', $path[0]);

// Bootstrap file.
$bootstrap = DRUPAL_ROOT . '/includes/bootstrap.inc';

if (!is_file($bootstrap)) {
  $errors[] = 'Cant locate bootstrap file "' . $bootstrap . '".';
}

else {

  // Drupal bootstrap.
  try {
    require_once $bootstrap;
    drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
  }
  catch (Exception $e) {
    $errors[] = 'Bootstrap faild.';
  }

  // Check that the main database is active.
  try {
    $row = db_select('variable', 'v')
            ->fields('v', array('value'))
            ->condition('name', 'maintenance_mode')
            ->execute()
            ->fetchObject();
    if (!$row) {
      $errors[] = 'Cant find data in variable table.';
    }
    elseif (unserialize($row->value) == '1') {
      $errors[] = 'Drupal off-line for maintenance.';
    }
  }
  catch (PDOException $e) {
    $errors[] = 'Master database not responding.';
  }
}

// Print all errors.
if ($errors) {
  $errors[] = 'Errors on this server will cause it to be removed from varnish backends.';
  header('HTTP/1.1 500 Internal Server Error');
  print implode("<br />\n", $errors);
}
else {
  // Set cache control, we dont wont to cache this page.
  header("Cache-Control: max-age=0, s-maxage=0");
  print 'OK';
}

// Exit immediately, note the shutdown function registered at the top of the file.
exit();
