<?php
/**
 * @file
 * Drupal Module: Varnish multi domain
 * Add support for updating multiple domains. its main purpose is to update
 * your real domain when your admin pages are located under a second secure url
 */

/**
 * Implements hook_expire_cache().
 *
 * Takes an array from the expire module, and issue purge. You may also safely
 * call this function directly with an array of local urls to purge.
 */
function varnish_multi_domain_expire_cache($paths) {

  $base = base_path();
  $host = _varnish_get_host();

  // Generate ban string, from path array.
  $purge = implode('$|^' . $base, $paths);
  $purge = '^' . $base . $purge . '$';

  // Loop through domains sett in configuratin.
  $varnish_multi_domain = variable_get('varnish_multi_domain', array(''));
  foreach ($varnish_multi_domain as $domain) {
    if (drupal_strlen($domain) > 4 && $host != $domain) {
      varnish_purge($domain, $purge);
    }
  }
}

/**
 * Implements hook_menu().
 */
function varnish_multi_domain_menu() {
  $items = array();
  $items['admin/config/development/varnish/multi_domain'] = array(
    'type'              => MENU_LOCAL_TASK,
    'title'             => 'multi domain',
    'page callback'     => 'drupal_get_form',
    'page arguments'   => array('varnish_multi_domain_admin_settings_form'),
    'access arguments' => array('administer varnish'),
  );
  return $items;
}

/**
 * Form function, called by drupal_get_form() in varnish_multi_domain_menu().
 */
function varnish_multi_domain_admin_settings_form($form, &$form_state) {

  $varnish_multi_domain = variable_get('varnish_multi_domain', array(''));
  // Add one extra at the end.
  $varnish_multi_domain = array_merge($varnish_multi_domain, array(''));

  $form['description'] = array(
    '#markup' => '<p>' . t('Add support for updating multiple domains.') . '</p>',
  );

  $form['varnish_multi_domain'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
  );

  foreach ($varnish_multi_domain as $key => $domain) {
    $form['varnish_multi_domain'][$key] = array(
      '#type' => 'textfield',
      '#title' => t('Domain %number', array('%number' => ($key + 1))),
      '#default_value' => $varnish_multi_domain[$key],
      '#size' => 150,
      '#maxlength' => 150,
      '#description' => t('eks: www.domain.no'),
      '#required' => FALSE,
    );
  }

  return system_settings_form($form);
}
